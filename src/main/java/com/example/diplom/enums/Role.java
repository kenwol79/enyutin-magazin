package com.example.diplom.enums;

public enum Role {

    ROLE_ADMIN,
    ROLE_USER;

    public static class Names {
        public static final String ROLE_ADMIN = "ROLE_ADMIN";
        public static final String ROLE_USER = "ROLE_USER";
    }

}
