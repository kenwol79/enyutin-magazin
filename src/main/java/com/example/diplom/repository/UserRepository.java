package com.example.diplom.repository;

import com.example.diplom.entity.ShopUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<ShopUser, Long> {

    Optional<ShopUser> findByLogin(String login);
}
