package com.example.diplom.controller;

import com.example.diplom.entity.Product;
import com.example.diplom.entity.ShopUser;
import com.example.diplom.repository.ProductRepository;
import com.example.diplom.repository.UserRepository;
import com.example.diplom.security.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class ProductController {

    private final ProductRepository productRepository;

    @PreAuthorize("permitAll()")
    @GetMapping("/products")
    public String getProducts(Model model, Authentication authentication){
        if (authentication != null) {
            UserDetailsImpl details = (UserDetailsImpl) authentication.getPrincipal();
            ShopUser user = details.getShopUser();
            model.addAttribute("login", user.getLogin());
            model.addAttribute("name", user.getName());
        } else {
            model.addAttribute("login", "Unauthorized");
            model.addAttribute("name", "Unauthorized");
        }

        List<Product> products = productRepository.findAll();
        model.addAttribute("products", products);
        return "products";
    }
}

