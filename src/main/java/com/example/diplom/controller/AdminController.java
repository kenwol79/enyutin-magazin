package com.example.diplom.controller;

import com.example.diplom.entity.Product;
import com.example.diplom.entity.ShopUser;
import com.example.diplom.enums.Role;
import com.example.diplom.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.security.RolesAllowed;

@Controller
@RequiredArgsConstructor
public class AdminController {

    private final UserRepository userRepository;

    @GetMapping("/admin")
    @RolesAllowed(Role.Names.ROLE_ADMIN)
    public String index() {
        return "admin";
    }

    @PostMapping("/admin/cart")
    @RolesAllowed(Role.Names.ROLE_ADMIN)
    public String saveToCart(@RequestParam long userId, @RequestParam long productId){
        ShopUser user = userRepository.getReferenceById(userId);
        user.getCart().add(new Product(productId));
        userRepository.save(user);
        return "redirect:/users";
    }


}

