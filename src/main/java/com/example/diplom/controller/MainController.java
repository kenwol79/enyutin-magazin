package com.example.diplom.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.security.PermitAll;

@Controller
public class MainController {

    @PermitAll
    @GetMapping("/")
    public String getMain(){
        return "index";
    }
}
