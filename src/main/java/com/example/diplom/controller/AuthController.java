package com.example.diplom.controller;

import com.example.diplom.entity.ShopUser;
import com.example.diplom.enums.Role;
import com.example.diplom.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class AuthController {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @GetMapping("/register")
    @PermitAll
    public String index(@RequestParam(required = false) String error, Model model) {
        if ("".equals(error)) {
            model.addAttribute("hasUser", true);
        }
        return "register";
    }

    @PostMapping("/register")
    @PermitAll
    public String register(@RequestParam String login,
                           @RequestParam String name,
                           @RequestParam String password) {

        Optional<ShopUser> shopUserCandidate = userRepository.findByLogin(login);

        if (shopUserCandidate.isPresent()) {
            return "redirect:/register?error";
        }

        String passwordHash = passwordEncoder.encode(password);

        ShopUser shopUser = ShopUser.builder()
                .name(name)
                .login(login)
                .passwordHash(passwordHash)
                .role(Role.ROLE_USER)
                .build();

        userRepository.save(shopUser);
        return "redirect:/login";
    }

}

