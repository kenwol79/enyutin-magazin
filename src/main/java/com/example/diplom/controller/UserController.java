package com.example.diplom.controller;

import com.example.diplom.entity.Product;
import com.example.diplom.entity.ShopUser;
import com.example.diplom.enums.Role;
import com.example.diplom.repository.UserRepository;
import com.example.diplom.security.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserRepository userRepository;

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/users")
    @RolesAllowed(Role.Names.ROLE_USER)
    public String getUsers(Model model, Authentication authentication) {
        UserDetailsImpl details = (UserDetailsImpl) authentication.getPrincipal();
        ShopUser user = userRepository.getReferenceById(details.getShopUser().getId());
        model.addAttribute("users", List.of(user));
        return "users";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/cart/save")
    @RolesAllowed(Role.Names.ROLE_USER)
    public String saveToCart(@RequestParam long productId, Authentication authentication) {
        UserDetailsImpl details = (UserDetailsImpl) authentication.getPrincipal();
        ShopUser user = userRepository.getReferenceById(details.getShopUser().getId());
        user.getCart().add(new Product(productId));
        userRepository.save(user);
        return "redirect:/users";
    }


}


