package com.example.diplom;

import com.example.diplom.service.DateService;
import com.example.diplom.service.LogService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class DateServiceTest {
    private final DateService dateService = new DateService();
    private final LogService logService = new LogService(dateService);

    @Test
    public void dateTest(){
        Date date = dateService.date();
        Assertions.assertNotNull(date);
    }

    @Test
    public void currentMonthTest(){
        String actual = dateService.getCurrentMonth();
        String expected = "NOVEMBER";
        Assertions.assertEquals(expected, actual);
    }
}
